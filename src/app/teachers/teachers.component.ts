import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-teachers',
  templateUrl: './teachers.component.html',
  styleUrls: ['./teachers.component.scss']
})
export class TeachersComponent implements OnInit {

  teachersform:FormGroup;
  selectedIndex2="";
  isEditBtnClicked2="no";
  doctorList:any=[];
  isSubmitted = true;
  constructor(private formBuilder:FormBuilder) 
  { 
      this.teachersform=this.formBuilder.group(
        {
            tname:['',[Validators.required]],
            tid:['',[Validators.required]],
            tcontact:['',[Validators.required]],
            taddress:['',[Validators.required]],
        }

      )
  }
  
    submit2(){
      this.isSubmitted = true;
    if(this.teachersform.valid)
    {
      console.log('submit',this.teachersform.value);
      this.doctorList.push(this.teachersform.value);
      
      // Code to add data in localstorage
      localStorage.setItem('doctorList',JSON.stringify(this.doctorList));
      
      Swal.fire({
        position: 'top-end',
        icon: 'success',
        title: 'Your details has been Submitted',
        showConfirmButton: false,
        timer: 1500
      });
      this.teachersform.reset();
      this.isSubmitted = false;
    }
      
    }
  
    clear2(){
      const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
          confirmButton: 'btn btn-success',
          cancelButton: 'btn btn-danger'
        },
        buttonsStyling: false
      })
      
      swalWithBootstrapButtons.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, cancel it!',
        cancelButtonText: 'No, cancel!',
        reverseButtons: true
      }).then((result) => {
        if (result.isConfirmed) {
          swalWithBootstrapButtons.fire(
            'Deleted!',
            'Your details has been deleted.',
            'success'
          )
          this.teachersform.reset();
        } else if (
          /* Read more about handling dismissals below */
          result.dismiss === Swal.DismissReason.cancel
        ) {
          swalWithBootstrapButtons.fire(
            'Cancelled',
            'Your details are safe :)',
            'error'
          )
        }
      })
      
      
    }
    edit2(idx:any){
      Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes'
      }).then((result) => {
        if (result.isConfirmed) {
          Swal.fire(
            'update!',
            'You can update.',
            
          )
        }
      })
      this.isEditBtnClicked2="yes";
      this.selectedIndex2 = idx;
      this.teachersform.patchValue(
        {
          tname: this.doctorList[idx].tname,
          tid: this.doctorList[idx].tid,
          tcontact: this.doctorList[idx].tcontact,
          taddress: this.doctorList[idx].taddress,
        }
      )
    }
  
    delete2(idx:any){
      const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
          confirmButton: 'btn btn-success',
          cancelButton: 'btn btn-danger'
        },
        buttonsStyling: false
      })
      
      swalWithBootstrapButtons.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: 'No, cancel!',
        reverseButtons: true
      }).then((result) => {
        if (result.isConfirmed) {
          swalWithBootstrapButtons.fire(
            'Deleted!',
            'Your file has been deleted.',
            'success'
          )
          this.doctorList.splice(idx,1);
         // Code to update data in localstorage
       localStorage.setItem('doctorList',JSON.stringify(this.doctorList))
        } else if (
          /* Read more about handling dismissals below */
          result.dismiss === Swal.DismissReason.cancel
        ) {
          swalWithBootstrapButtons.fire(
            'Cancelled',
            'Your imaginary file is safe :)',
            'error'
          )
        }
      })
      
    }
  
    update2(){
      Swal.fire({
        title: 'Are you sure, you want to update?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, update it!'
      }).then((result) => {
        if (result.isConfirmed) {
          Swal.fire(
            'Updated!',
            'Your details has been updated.',
            'success'
          )
        }
      })
      this.doctorList[this.selectedIndex2].tname = this.teachersform.value.tname;
      this.doctorList[this.selectedIndex2].tid = this.teachersform.value.tid;
      this.doctorList[this.selectedIndex2].tcontact = this.teachersform.value.tcontact;
      this.doctorList[this.selectedIndex2].taddress = this.teachersform.value.taddress;
     // Code to update data in localstorage
     localStorage.setItem('doctorList',JSON.stringify(this.doctorList))
      this.teachersform.reset();
      this.isEditBtnClicked2="no";
    }
    ngOnInit(): void {
     // Code to read data from localstorage
     let data = localStorage.getItem('doctorList');
     this.doctorList = JSON.parse(data || '');
   }
  
    
}
