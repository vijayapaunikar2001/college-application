import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-exams',
  templateUrl: './exams.component.html',
  styleUrls: ['./exams.component.scss']
})
export class ExamsComponent implements OnInit {

  examform:FormGroup;
  selectedIndex5="";
  isEditBtnClicked5="no";
  billList:any=[];
  isSubmitted = true;

  constructor(private formBuilder:FormBuilder) 
  { 
      this.examform=this.formBuilder.group(
        {
            ename:['',[Validators.required]],
            eprob:['',[Validators.required]],
            econtact:['',[Validators.required]],
            efees:['',[Validators.required]],
        }

      )
  }
  
    submit5(){
      this.isSubmitted = true;
    if(this.examform.valid)
    {
      console.log('submit',this.examform.value);
      this.billList.push(this.examform.value);
      
      // Code to add data in localstorage
      localStorage.setItem('billList',JSON.stringify(this.billList));
      
      Swal.fire({
        position: 'top-end',
        icon: 'success',
        title: 'Your details has been Submitted',
        showConfirmButton: false,
        timer: 1500
      });
      this.examform.reset();
      this.isSubmitted = false;
    }
      
    }
    clear5(){
      const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
          confirmButton: 'btn btn-success',
          cancelButton: 'btn btn-danger'
        },
        buttonsStyling: false
      })
      
      swalWithBootstrapButtons.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, cancel it!',
        cancelButtonText: 'No, cancel!',
        reverseButtons: true
      }).then((result) => {
        if (result.isConfirmed) {
          swalWithBootstrapButtons.fire(
            'Deleted!',
            'Your details has been deleted.',
            'success'
          )
          this.examform.reset();
        } else if (
          /* Read more about handling dismissals below */
          result.dismiss === Swal.DismissReason.cancel
        ) {
          swalWithBootstrapButtons.fire(
            'Cancelled',
            'Your details are safe :)',
            'error'
          )
        }
      })
      
      
    }
    edit5(idx:any){
      Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes'
      }).then((result) => {
        if (result.isConfirmed) {
          Swal.fire(
            'update!',
            'You can update.',
            
          )
        }
      })
      this.isEditBtnClicked5="yes";
      this.selectedIndex5 = idx;
      this.examform.patchValue(
        {
          ename: this.billList[idx].ename,
          econtact: this.billList[idx].econtact,
          eprob: this.billList[idx].eprob,
          efees: this.billList[idx].efees,
        }
      )
    }
  
    delete5(idx:any){
      const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
          confirmButton: 'btn btn-success',
          cancelButton: 'btn btn-danger'
        },
        buttonsStyling: false
      })
      
      swalWithBootstrapButtons.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: 'No, cancel!',
        reverseButtons: true
      }).then((result) => {
        if (result.isConfirmed) {
          swalWithBootstrapButtons.fire(
            'Deleted!',
            'Your file has been deleted.',
            'success'
          )
          this.billList.splice(idx,1);
           // Code to delete data in localstorage
      localStorage.setItem('billList', JSON.stringify(this.billList))
        } else if (
          /* Read more about handling dismissals below */
          result.dismiss === Swal.DismissReason.cancel
        ) {
          swalWithBootstrapButtons.fire(
            'Cancelled',
            'Your imaginary file is safe :)',
            'error'
          )
        }
      })
      
    }
  
    update5(){
      Swal.fire({
        title: 'Are you sure, you want to update?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, update it!'
      }).then((result) => {
        if (result.isConfirmed) {
          Swal.fire(
            'Updated!',
            'Your details has been updated.',
            'success'
          )
        }
      })
      this.billList[this.selectedIndex5].ename = this.examform.value.ename;
      this.billList[this.selectedIndex5].econtact = this.examform.value.econtact;
      this.billList[this.selectedIndex5].eprob = this.examform.value.eprob;
      this.billList[this.selectedIndex5].efees = this.examform.value.efees;
      
      // Code to update data in localstorage
      localStorage.setItem('billList', JSON.stringify(this.billList))
      this.examform.reset();
      this.isEditBtnClicked5="no";
    }
    ngOnInit(): void {
      // Code to read data from localstorage
      let data = localStorage.getItem('billList');
      this.billList = JSON.parse(data || '');

        
    }
  
    
}
