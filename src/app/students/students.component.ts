import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-students',
  templateUrl: './students.component.html',
  styleUrls: ['./students.component.scss']
})
export class StudentsComponent implements OnInit {
  
  studentform:FormGroup;
  selectedIndex3="";
  isEditBtnClicked3="no";
  patientList:any=[];

  isSubmitted = true;
  constructor(private formBuilder:FormBuilder) 
  { 
      this.studentform=this.formBuilder.group(
        {
            sname:['',[Validators.required]],
            sprob:['',[Validators.required]],
            scontact:['',[Validators.required]],
            saddress:['',[Validators.required]],
        }

      )
  }
  
    submit3(){
      this.isSubmitted = true;
    if(this.studentform.valid)
    {
      console.log('submit',this.studentform.value);
      this.patientList.push(this.studentform.value);
      
      // Code to add data in localstorage
      localStorage.setItem('doctorList',JSON.stringify(this.patientList));
      
      Swal.fire({
        position: 'top-end',
        icon: 'success',
        title: 'Your details has been Submitted',
        showConfirmButton: false,
        timer: 1500
      });
      this.studentform.reset();
      this.isSubmitted = false;
    }
      
    }
    clear3(){
      const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
          confirmButton: 'btn btn-success',
          cancelButton: 'btn btn-danger'
        },
        buttonsStyling: false
      })
      
      swalWithBootstrapButtons.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, cancel it!',
        cancelButtonText: 'No, cancel!',
        reverseButtons: true
      }).then((result) => {
        if (result.isConfirmed) {
          swalWithBootstrapButtons.fire(
            'Deleted!',
            'Your details has been deleted.',
            'success'
          )
          this.studentform.reset();
        } else if (
          /* Read more about handling dismissals below */
          result.dismiss === Swal.DismissReason.cancel
        ) {
          swalWithBootstrapButtons.fire(
            'Cancelled',
            'Your details are safe :)',
            'error'
          )
        }
      })
      
      
    }
    edit3(idx:any){
      Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes'
      }).then((result) => {
        if (result.isConfirmed) {
          Swal.fire(
            'update!',
            'You can update.',
            
          )
        }
      })
      this.isEditBtnClicked3="yes";
      this.selectedIndex3 = idx;
      this.studentform.patchValue(
        {
          sname: this.patientList[idx].sname,
          scontact: this.patientList[idx].scontact,
          sprob: this.patientList[idx].sprob,
          saddress: this.patientList[idx].saddress,
        }
      )
    }
  
    delete3(idx:any){
      const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
          confirmButton: 'btn btn-success',
          cancelButton: 'btn btn-danger'
        },
        buttonsStyling: false
      })
      
      swalWithBootstrapButtons.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: 'No, cancel!',
        reverseButtons: true
      }).then((result) => {
        if (result.isConfirmed) {
          swalWithBootstrapButtons.fire(
            'Deleted!',
            'Your file has been deleted.',
            'success'
          )
          this.patientList.splice(idx,1);
          // Code to update data in localstorage
      localStorage.setItem('patientList',JSON.stringify(this.patientList))
        } else if (
          /* Read more about handling dismissals below */
          result.dismiss === Swal.DismissReason.cancel
        ) {
          swalWithBootstrapButtons.fire(
            'Cancelled',
            'Your imaginary file is safe :)',
            'error'
          )
        }
      })
      
    }
  
    update3(){
      Swal.fire({
        title: 'Are you sure, you want to update?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, update it!'
      }).then((result) => {
        if (result.isConfirmed) {
          Swal.fire(
            'Updated!',
            'Your details has been updated.',
            'success'
          )
        }
      })
      this.patientList[this.selectedIndex3].sname = this.studentform.value.sname;
      this.patientList[this.selectedIndex3].scontact = this.studentform.value.scontact;
      this.patientList[this.selectedIndex3].sprob = this.studentform.value.sprob;
      this.patientList[this.selectedIndex3].saddress = this.studentform.value.saddress;
      
      // Code to update data in localstorage
      localStorage.setItem('patientList',JSON.stringify(this.patientList))
      this.studentform.reset();
      this.isEditBtnClicked3="no";
    }
    ngOnInit(): void {
      // Code to read data from localstorage
      let data = localStorage.getItem('patientist');
      this.patientList = JSON.parse(data || '');
        
    }
  
    
}
